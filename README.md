This is MATE Menu, a fork of [MintMenu](https://github.com/linuxmint/mintmenu).

  * MATE Menu removes the Mint specific search options.
  * Any features that are dependant on Ubuntu Software Center and/or
  Synaptic are only presented in the menus when the required
  applications are installed and execution via `gksu` and `sudo` has
  been replaced with PolicyKit.
  * File, directory and package names have been changed to prevent
  conflicts and I've brought in some components from [mint-common](https://github.com/linuxmint/mint-common)
  so that the MateMenu package can stand alone.
  * You can install this version from [my PPA](https://launchpad.net/~brawl98/+archive/ubuntu/ppa)